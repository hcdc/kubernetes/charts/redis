
{{- define "redis.podTemplate" -}}

metadata:
  creationTimestamp: null
  labels:
    {{- include "redis.selectorLabels" . | nindent 4 }}
    io.kompose.service: {{ .Values.baseName }}
  annotations:

spec:
  affinity: {{- include "redis.affinity" . | nindent 4 }}
  {{- if .Values.useManyFilesPVC }}
  securityContext:
    fsGroupChangePolicy: OnRootMismatch
    seLinuxOptions:
      type: spc_t
  serviceAccountName: manyfilespvc
  serviceAccount: manyfilespvc
  imagePullSecrets:
    - name: deployer-quay-{{ .Values.global.openshiftCluster }}
  {{- end }}
  containers:
    - name: {{ .Values.baseName }}
      {{- if .Values.command }}
      command:
        {{- range .Values.command }}
        - {{ . | quote }}
        {{- end }}
      {{- end }}
      image: "{{ printf "%s-quay-registry.apps.%s.fzg.local" (regexFind "dev|prod" .Values.global.openshiftCluster) .Values.global.openshiftCluster }}/{{ required "You need to specify a cluster" .Values.global.openshiftCluster }}_{{ .Release.Namespace }}/{{ required "A source image (fromImage) for redis is required!" .Values.fromImage }}"
      ports:
        - containerPort: 6379
      {{- $builtinResources := include "redis.builtinResources" . | fromYaml }}
      {{- $mergedResources := mustMergeOverwrite (dict) $builtinResources .Values.resources }}
      resources: {{ toYaml $mergedResources | nindent 8 }}
  restartPolicy: Always
{{- end }}
